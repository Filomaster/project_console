﻿using System;


namespace Project_CONSOLE
{
    
    class Game_Area
    {
        int area_start_pos_x;
        int area_start_pos_y;
        int area_end_pos_x;
        int area_end_pos_y;

        int area_width;
        int area_height;
        Random rnd = new Random();
        public Game_Object[] wall;

        public void set_area_dim(int start_x, int start_y, int end_x, int end_y)
        {
            this.area_start_pos_x = start_x;
            this.area_start_pos_y = start_y;
            this.area_end_pos_x = end_x;
            this.area_end_pos_y = end_y;
        }

        public void generate_random_area(int quantity)
        {
            int wall_pos_x;
            int wall_pos_y;
            wall = new Game_Object[quantity];

            for (int i = 0; i < quantity; i++)
            { 
                wall_pos_x = rnd.Next(this.area_start_pos_x+1, this.area_end_pos_x-1);
                wall_pos_y = rnd.Next(this.area_start_pos_y+1, this.area_end_pos_y-1);
                wall[i] = new Game_Object(wall_pos_x, wall_pos_y);
                Console.SetCursorPosition(wall_pos_x, wall_pos_y);
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.Write("#");
                Console.ResetColor();
            }
        }
        public void regenerate_area(int quantity)
        {
            for (int i = 0; i < quantity; i++)
            {
                Console.SetCursorPosition(wall[i].get_x_pos(), wall[i].get_y_pos());
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.Write("#");
                Console.ResetColor();
            }

        }

        //add getters
        public int get_area_start_pos_x()
        {
            return this.area_start_pos_x;
        }
        public int get_area_start_pos_y()
        {
            return this.area_start_pos_y;
        }
        public int get_area_end_pos_x()
        {
            return this.area_end_pos_x;
        }
        public int get_area_end_pos_y()
        {
            return this.area_end_pos_y;
        }
    }
}
