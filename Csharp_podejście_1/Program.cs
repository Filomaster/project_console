﻿using System;
using System.Diagnostics;

namespace Project_CONSOLE
{
    class Program  //Rozważ zmianę nazwy na 'Executable'
    {
        private static int max_width = 120;
        private static int max_height = 30;

        private static int player_initial_x = 4;
        private static int player_initial_y = 4;
        

        static void Main(string[] args)
        {

            Player player = new Player(player_initial_x, player_initial_y);
            UI game_ui = new UI();
            Game game = new Game();
            Game_Area area = new Game_Area();
            Collider player_collider = new Collider();
            
            // -------------- INITIAL SETTINGS ----------
            //Console settings
            Console.Title = "Test";
            Console.CursorVisible = false; 
            Console.SetWindowSize(max_width, max_height);
            Console.SetBufferSize(max_width, max_height);
            //Variables
            bool is_running = true;
            bool collisions = true;
            char printing_char = ' ';
            int wall_count = 737;  //Initial value == 737
            ////Constructors
            game.start_game();
            player.initial_position(4, 4);
            area.set_area_dim(0, 2, max_width, max_height-1);
            // ---------------------------------------------

            Console.Clear();
            game_ui.draw_UI();
            area.generate_random_area(wall_count);
            do //Main loop (temp)
            {
                if (collisions == true)
                {
                    for (int i = 0; i < wall_count; i++)
                    {
                        //Debug.Write("Run: [" + i + "] - ");
                        player_collider.check(player, area.wall[i]);
                        if (player_collider.get_trigger() == true)
                        {
                            Debug.Write("Collision detected at [" + i + "] - (" + area.wall[i].get_x_pos() + "," + area.wall[i].get_y_pos() + "); ");
                            player.set_pos(player.get_old_x_pos(), player.get_old_y_pos());
                            Debug.WriteLine("Moved player to: (" + player.get_old_x_pos() + "," + player.get_old_y_pos() + ")");
                            player_collider.reset_trigger();
                        }

                    }
                }
                Console.SetCursorPosition(player.get_old_x_pos(), player.get_old_y_pos());
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write(printing_char);
                Console.ResetColor();
                player.set_old_pos(player.get_x_pos(), player.get_y_pos());
                Console.SetCursorPosition(player.get_x_pos(), player.get_y_pos());
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.Write("O");
                Console.ResetColor();
                Console.SetCursorPosition(player.get_x_pos(), player.get_y_pos());

                game_ui.player_cords(player.get_x_pos(), player.get_y_pos());
                //Debug.WriteLine("Current player position: X:" + player.x_pos + " Y:" + player.y_pos);

                //int test = 1;
                //Debug.WriteLine("Test is " + (test > 0 ? "positive" : "negative"));
                //inline if Zachowaj, bo może się przydać :P To jest template niego tak btw

                //for(int n =0; n <5; n++)
                //Debug.WriteLine("Hello"[n]);
                //Lol, to wypisuje literkę w każdej linii osobno


                
                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.UpArrow:
                        player.set_pos(player.get_x_pos(), player.get_y_pos()-1);
                        break;
                    case ConsoleKey.DownArrow:
                        player.set_pos(player.get_x_pos(), player.get_y_pos() + 1);
                        break;
                    case ConsoleKey.RightArrow:
                        player.set_pos(player.get_x_pos()+1, player.get_y_pos());
                        break;
                    case ConsoleKey.LeftArrow:
                        player.set_pos(player.get_x_pos()-1, player.get_y_pos());
                        break;

                    case ConsoleKey.F1:
                        Console.Clear();
                        collisions = true;
                        game_ui.draw_UI();
                        area.generate_random_area(wall_count);
                        break;

                    case ConsoleKey.F2:
                        Console.Clear();
                        collisions = true;
                        game_ui.draw_UI();
                        area.regenerate_area(wall_count);
                        break;

                    case ConsoleKey.F3:
                        if(printing_char == ' ')
                        {
                            printing_char = '*';
                        }else if(printing_char == '*')
                        {
                            printing_char = ' ';
                        }
                        break;

                    case ConsoleKey.F4:
                        Console.Clear();
                        collisions = false;
                        break;

                    case ConsoleKey.F5:
                        Console.Clear();
                        game_ui.help_menu();
                        Console.Read();
                        Console.Clear();
                        area.regenerate_area(wall_count);
                        break;
                    case ConsoleKey.Escape:
                        is_running = false;
                        break;
                }

                int tmp_x;
                int tmp_y;

                //Temporary game area boundry
                if (player.get_x_pos() < area.get_area_start_pos_x()+1)
                {
                    tmp_x = area.get_area_start_pos_x()+1;
                    player.set_pos(tmp_x, player.get_y_pos());
                }else if( player.get_x_pos() > area.get_area_end_pos_x()-2)
                {
                    tmp_x = area.get_area_end_pos_x() - 2;
                    player.set_pos(tmp_x, player.get_y_pos());
                }
                if(player.get_y_pos() < area.get_area_start_pos_y()+1)
                {
                    tmp_y = area.get_area_start_pos_y()+1;
                    player.set_pos(player.get_x_pos(), tmp_y);
                }
                else if(player.get_y_pos() > area.get_area_end_pos_y() - 2)
                {
                    tmp_y = area.get_area_end_pos_y() - 2;
                    player.set_pos(player.get_x_pos(), tmp_y);
                }

                
            
                
            } while (is_running);
        }
    }
}
