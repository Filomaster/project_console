﻿using System;

namespace Project_CONSOLE
{
    class UI
    {

        private void draw_horizontal_line()
        {
            for (int i = 0; i < Console.BufferWidth; i++)
            {
                if (i == 0 || i + 1 == Console.BufferWidth)
                {
                    Console.Write("|");
                }
                else
                {
                    Console.Write("-");
                }
            }
        }
        private void draw_vertical_lines(int height)
        {
            for (int j = 0; j < height; j++)
            {


                for (int i = 0; i < Console.BufferWidth; i++)
                {
                    if (i == 0 || i + 1 == Console.BufferWidth)
                    {
                        Console.Write("|");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
            }
        }

        public void draw_UI()
        {
            draw_horizontal_line();
            draw_vertical_lines(1);
            draw_horizontal_line();
            draw_vertical_lines(Console.BufferHeight - Console.CursorTop-2);
            draw_horizontal_line();
            
            

        }
        public void help_menu()
        {
            Console.SetCursorPosition((Console.BufferWidth / 2)-4, Console.BufferHeight / 3);
            Console.WriteLine("MENU POMOCY");

            
        }

        //póki co tymczasowa funkcja
        public void player_cords(int x, int y)
        {
            Console.SetCursorPosition(1, 1);
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.Write("Player position: (" + x + "," + y + ")    ");
            Console.ResetColor();
        }
    }
}
