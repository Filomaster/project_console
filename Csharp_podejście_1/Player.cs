﻿using System;


namespace Project_CONSOLE
{
    class Player : Game_Object
    {
        private int x_pos;
        private int y_pos;
        private int old_x_pos;
        private int old_y_pos;
        
        public Player(int x, int y)
            :   base(x, y)
        {

        }

        public void initial_position(int x, int y)
            {
            this.x_pos = x;
            this.y_pos = y;
            this.old_x_pos = x;
            this.old_y_pos = y;
            }
        //public void set_x_pos(int x_pos)
        //{
        //    this.x_pos = x_pos;
        //}
        //public void set_y_pos(int y_pos)
        //{
        //    this.y_pos = y_pos;
        //}

        //public int get_x_pos()
        //{
        //    return this.x_pos;
        //}
        //public int get_y_pos()
        //{
        //    return this.y_pos;
        //}
        public void set_old_pos(int x, int y)
        {
            this.old_x_pos = x;
            this.old_y_pos = y;
        }
        public int get_old_x_pos()
        {
            return this.old_x_pos;
        }
        public int get_old_y_pos()
        {
            return this.old_y_pos;
        }

    }
}
