﻿using System;



namespace Project_CONSOLE
{
    class Game_Object
    {
        private int x_pos;
        private int y_pos;

        public Game_Object(int x, int y)
        {
            this.x_pos = x;
            this.y_pos = y;
        }

       public int get_x_pos()
        {
            return this.x_pos;
        }

        public int get_y_pos()
        {
            return this.y_pos;
        }

        public void set_pos(int x, int y)
        {
            this.x_pos = x;
            this.y_pos = y;
        }
    }
}
